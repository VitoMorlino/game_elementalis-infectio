// Fill out your copyright notice in the Description page of Project Settings.

#include "GFPSAICharacter.h"
#include "GFPSAIController.h"
#include "GFPSCharacter.h"
#include "TimerManager.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "VitosLogMacros.h"
#include "CustomTypes.h"
#include "GFPSBaseDamageType.h"
#include "Components/SkeletalMeshComponent.h"
#include "UObject/ConstructorHelpers.h"

//Component Includes
#include "GFPSCharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/AudioComponent.h"
#include "Perception/PawnSensingComponent.h" 


// Sets default values
AGFPSAICharacter::AGFPSAICharacter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Do not log debug messages by default...change this value per-instance, as necessary
	bLogDebugMessages = false;

	// Try to set DefaultPawnClass to the Blueprinted class, but default to the base c++ class if not found 
	static ConstructorHelpers::FClassFinder<AAIController> AIConBPClass(TEXT("/Game/Blueprints/Viruses/AI/BP_AIController"));
	AIControllerClass = AIConBPClass.Class ? AIConBPClass.Class : TSubclassOf<AAIController>(AGFPSAIController::StaticClass());

	bUseControllerRotationYaw = false; //Required to be false to smoothly turn character to Controller->ControlRotation
	GetCharacterMovement()->bOrientRotationToMovement = true; //Orient rotation of character to direction of acceleration
	GetCharacterMovement()->bUseControllerDesiredRotation = true; //Rotate the character to the rotation of the controller
	GetCharacterMovement()->bUseRVOAvoidance = true; //Avoid obstacles when generating path-following data
	GetCharacterMovement()->MaxAcceleration = 500.f; // (default 2048.0f)
	GetCharacterMovement()->MaxWalkSpeed = 150.f;

	/* Our sensing component to detect players by visibility and noise checks. */
	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));
	PawnSensingComp->SetPeripheralVisionAngle(60.0f);
	PawnSensingComp->SightRadius = 2000;
	PawnSensingComp->HearingThreshold = 600;
	PawnSensingComp->LOSHearingThreshold = 1200;

	/* Ignore this channel or it will absorb the trace impacts instead of the skeletal mesh */
	//GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	GetCapsuleComponent()->SetCapsuleHalfHeight(96.0f, false);
	GetCapsuleComponent()->SetCapsuleRadius(42.0f);

	/* These values are matched up to the CapsuleComponent above and are used to find navigation paths */
	GetMovementComponent()->NavAgentProps.AgentRadius = 42;
	GetMovementComponent()->NavAgentProps.AgentHeight = 192;

	MeleeCollisionComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("MeleeCollision"));
	MeleeCollisionComp->SetRelativeLocation(FVector(45, 0, 25));
	MeleeCollisionComp->SetCapsuleHalfHeight(60);
	MeleeCollisionComp->SetCapsuleRadius(35, false);
	MeleeCollisionComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	MeleeCollisionComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	MeleeCollisionComp->SetupAttachment(GetCapsuleComponent());

	AudioLoopComp = CreateDefaultSubobject<UAudioComponent>(TEXT("LoopedSoundComp"));
	AudioLoopComp->bAutoActivate = false;
	AudioLoopComp->bAutoDestroy = false;
	AudioLoopComp->SetupAttachment(RootComponent);

	Health = 50.f;
	SprintingSpeedMultiplier = 2.f;
	MeleeDamageAmount = 10.0f;
	MeleeStrikeCooldown = 2.0f;
	MeleeDamageType = UGFPSBaseDamageType::StaticClass();

	BotType = EBotBehaviorType::Idle;
	BotAggressiveness = EBotAggressiveness::PassiveAggressive;

	SenseTimeOut = 2.5f;
	bShouldWander = false;
	MaxWanderDistance = 200.f;

	/* Note: Visual Setup is done in the AICharacter Blueprint file */
}

// Called when the game starts or when spawned
void AGFPSAICharacter::BeginPlay()
{
	Super::BeginPlay();

	/* This is the earliest moment we can bind our delegates to the component */
	if (PawnSensingComp)
	{
		PawnSensingComp->OnSeePawn.AddDynamic(this, &AGFPSAICharacter::OnSeePawn);
		PawnSensingComp->OnHearNoise.AddDynamic(this, &AGFPSAICharacter::OnHearNoise);
	}
	if (MeleeCollisionComp)
	{
		MeleeCollisionComp->OnComponentBeginOverlap.AddDynamic(this, &AGFPSAICharacter::OnMeleeCompBeginOverlap);
	}

	UpdateAudioLoop(bSensedTarget);

	InitialLocation = GetActorLocation();
}

// Called every frame
void AGFPSAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/* Print on-screen debug messages while timers are active */
	if (bLogDebugMessages)
	{
		/* MeleeAttack Timer is active */
		if (GetWorldTimerManager().IsTimerActive(TimerHandle_MeleeAttack))
		{
			SCREENMSG_F(.5f, CYAN, "MeleeAttack_TimeRemaining:",
				GetWorldTimerManager().GetTimerRemaining(TimerHandle_MeleeAttack));
		}

		/* SenseTimeOut Timer is active */
		if (GetWorldTimerManager().IsTimerActive(TimerHandle_SenseTimeOut))
		{
			SCREENMSG_F(.5f, YELLOW, "SenseTimeOut_TimeRemaining:",
				GetWorldTimerManager().GetTimerRemaining(TimerHandle_SenseTimeOut));
		}
	}

}

bool AGFPSAICharacter::IsSprinting() const
{
	/* Allow NPC to sprint when they have seen a player */
	return bSensedTarget && !GetVelocity().IsZero();
}

float AGFPSAICharacter::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	/* IF BotAggressiveness = Passive, take no damage */
	if (BotAggressiveness == EBotAggressiveness::Passive)
	{
		if (bLogDebugMessages) LOGMSG(NonPlayerChar, Warning, "BotAggressiveness = Passive...NPC cannot be attacked");

		return 0.0f;
	}
	else
	{
		AGFPSAIController* AICon = Cast<AGFPSAIController>(GetController());
		AGFPSBaseCharacter* InstigatorChar = EventInstigator ? Cast<AGFPSBaseCharacter>(EventInstigator->GetCharacter()) : nullptr;
		APawn* ExistingTarget = AICon ? AICon->GetTargetEnemy() : nullptr;

		/* IF TargetEnemy is not set already, set TargetEnemy to the InstigatorChar */
		if (AICon && InstigatorChar && !ExistingTarget)
		{
			if (InstigatorChar->IsAlive())
			{
				if (bLogDebugMessages) LOGMSG(NonPlayerChar, Warning, "NPC was attacked! Setting Target Enemy to InstigatorChar");

				AICon->SetTargetEnemy(InstigatorChar);
			}
			else
			{
				if (bLogDebugMessages) LOGMSG(NonPlayerChar, Warning, "InstigatorChar is Dead...cannot set target");
			}
		}

		/*************************************************************************************************************/
		/********* Debugging Log Messages ****************************************************************************/
		/*                                                                                                           */
		if (bLogDebugMessages)
		{
			if (!AICon) LOGMSG(NonPlayerChar, Error, "AICon not found, check AIController class");

			if (!EventInstigator) LOGMSG(NonPlayerChar, Error, "EventInstigator not found...Is it being set in the damage call?");

			if (!InstigatorChar && EventInstigator) LOGMSG(NonPlayerChar, Error, "EventInstigator was not a BaseCharacter");

			if (ExistingTarget) LOGMSG(NonPlayerChar, Error, "AICon already has an ExistingTarget");
		}
		/*                                                                                                           */
		/********* End Debugging Log Messages ************************************************************************/
		/*************************************************************************************************************/
	}

	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

/***************************************************************************************************************************/
/***** AI ***** AI ***** AI ***** AI ***** AI ***** AI ***** AI ***** AI ***** AI ***** AI ***** AI ***** AI ***** AI ******/
/***************************************************************************************************************************/
/*                                                                                                                         */

void AGFPSAICharacter::SetBotType(EBotBehaviorType NewType)
{
	BotType = NewType;

	AGFPSAIController* AICon = Cast<AGFPSAIController>(GetController());
	if (AICon)
	{
		AICon->SetBlackboardBotType(NewType);
	}

	UpdateAudioLoop(bSensedTarget);
}

void AGFPSAICharacter::SetBotAggressiveness(EBotAggressiveness NewAgro)
{
	BotAggressiveness = NewAgro;

	AGFPSAIController* AICon = Cast<AGFPSAIController>(GetController());
	if (AICon)
	{
		AICon->SetBlackboardBotAgro(NewAgro);
	}
}

void AGFPSAICharacter::SetShouldWander(bool bNewWander)
{
	bShouldWander = bNewWander;

	AGFPSAIController* AICon = Cast<AGFPSAIController>(GetController());
	if (AICon)
	{
		AICon->SetBlackboardShouldWander(bNewWander);
	}
}

/***************************************************************************************************************************/
/***** Pawn Sensing ***** Pawn Sensing ***** Pawn Sensing ***** Pawn Sensing ***** Pawn Sensing ***** Pawn Sensing *********/
/***************************************************************************************************************************/
/*                                                                                                                         */

void AGFPSAICharacter::OnSeePawn(APawn* SensedPawn)
{
	/* Do nothing if this NPC is not alive or is Passive */
	if (!IsAlive() || BotAggressiveness == EBotAggressiveness::Passive)
	{
		return;
	}

	/* Return Error if AIController is not found */
	AGFPSAIController* AICon = Cast<AGFPSAIController>(GetController());
	if (!AICon)
	{
		if (bLogDebugMessages) LOGMSG(NonPlayerChar, Error, "AIController not found...Check AIController class");

		return;
	}

	AGFPSBaseCharacter* SensedBaseChar = Cast<AGFPSBaseCharacter>(SensedPawn);

	if (SensedBaseChar && SensedBaseChar->IsAlive())
	{
		/* IF the SensedPawn is a Player Character */
		if (SensedBaseChar->IsA(AGFPSCharacter::StaticClass()))
		{
			if (bLogDebugMessages) LOGMSG(NonPlayerChar, Warning, "NPC saw a Player Character");

			/* If this NPC already has a target other than SensedBaseChar, ignore SensedBaseChar */
			if (AICon->GetTargetEnemy() && AICon->GetTargetEnemy() != SensedBaseChar)
			{
				return;
			}

			/** If this NPC is PassiveAggressive and doesn't already have the SensedBaseChar as a target,
			*	ignore it because PassiveAggressive NPCs shouldn't attack unless provoked */
			if (BotAggressiveness == EBotAggressiveness::PassiveAggressive &&
				AICon->GetTargetEnemy() != SensedBaseChar)
			{
				return;
			}

			if (!bSensedTarget)
			{
				UpdateAudioLoop(true);

				AICon->SetTargetEnemy(SensedBaseChar);
			}
			bSensedTarget = true;

			/* Set timer to call LostTarget when SenseTimeOut expires */
			GetWorldTimerManager().SetTimer(
				TimerHandle_SenseTimeOut, this, &AGFPSAICharacter::LostTarget, SenseTimeOut, false);

		}
		/* ELSE IF the SensedPawn is another AI Character */
		else if (SensedBaseChar->IsA(AGFPSAICharacter::StaticClass()))
		{
			if (bLogDebugMessages) LOGMSG(NonPlayerChar, Log, "NPC saw another AI Character");
		}
		/* ELSE the SensedPawn is neither a PlayerCharacter nor an AICharacter */
		else
		{
			if (bLogDebugMessages) LOGMSG(NonPlayerChar, Warning, "NPC saw an unrecognized Pawn");
		}
	}
	else //if (!SensedBaseChar || !SensedBaseChar->IsAlive())
	{
		if (!SensedBaseChar)
		{
			if (bLogDebugMessages) LOGMSG(NonPlayerChar, Warning, "SensedPawn is not a BaseCharacter");
		}

		if (!SensedBaseChar->IsAlive())
		{
			if (bLogDebugMessages) LOGMSG(NonPlayerChar, Log, "SensedPawn is not alive");
		}
	}
}

void AGFPSAICharacter::OnHearNoise(APawn* SensedPawn, const FVector& Location, float Volume)
{
	/* Do nothing if this NPC is not alive */
	if (!IsAlive())
	{
		return;
	}

	if (!bSensedTarget)
	{
		UpdateAudioLoop(true);
	}
	bSensedTarget = true;

	AGFPSAIController* AICon = Cast<AGFPSAIController>(GetController());
	if (AICon)
	{
		/* Set timer to call LostTarget when SenseTimeOut expires */
		GetWorldTimerManager().SetTimer(
			TimerHandle_SenseTimeOut, this, &AGFPSAICharacter::LostTarget, SenseTimeOut, false);

		AICon->SetTargetEnemy(SensedPawn);
	}
}

void AGFPSAICharacter::LostTarget_Implementation()
{
	if (bLogDebugMessages) LOGMSG(NonPlayerChar, Log, "SenseTimeOut expired");

	bSensedTarget = false;

	/* Stop playing the chasing sound */
	UpdateAudioLoop(false);

	AGFPSAIController* AICon = Cast<AGFPSAIController>(GetController());

	if (AICon)
	{
		/* Clear TargetEnemy */
		AICon->SetTargetEnemy(nullptr);
	}

	//@??? Clear timer?
}

/***************************************************************************************************************************/
/***** Attacking ***** Attacking ***** Attacking ***** Attacking ***** Attacking ***** Attacking ***** Attacking ***********/
/***************************************************************************************************************************/
/*                                                                                                                         */

void AGFPSAICharacter::OnMeleeCompBeginOverlap(
	UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	/* Return Error if AIController is not found */
	AGFPSAIController* AICon = Cast<AGFPSAIController>(GetController());
	if (!AICon)
	{
		if (bLogDebugMessages) LOGMSG(NonPlayerChar, Error, "AIController not found...Check AIController class");

		return;
	}

	/** If this NPC is PassiveAggressive and doesn't already have the OtherActor as a target,
	*	ignore it because PassiveAggressive NPCs shouldn't attack unless provoked */
	if (BotAggressiveness == EBotAggressiveness::PassiveAggressive &&
		AICon->GetTargetEnemy() != OtherActor)
	{
		return;
	}

	/* Perform a melee attack if this NPC is not Passive */
	if (BotAggressiveness != EBotAggressiveness::Passive)
	{
		PerformMeleeStrikeOn(OtherActor);
	}
}

void AGFPSAICharacter::PerformMeleeStrikeOn(AActor* ActorToHit)
{
	if (GetWorldTimerManager().IsTimerActive(TimerHandle_MeleeAttack))
	{
		if (bLogDebugMessages) LOGMSG(NonPlayerChar, Warning, "TimerHandle_MeleeAttack is active...Cannot attack again so soon");

		return;
	}

	if (IsAlive() && ActorToHit && ActorToHit != this)
	{
		if (!ActorToHit)
		{
			LOGMSG(NonPlayerChar, Warning, "ActorToHit was null");
			return;
		}

		//Don't hit other AICharacters
		if (ActorToHit->IsA(AGFPSAICharacter::StaticClass()) /*&& !bEnableFriendlyFire*/)
		{
			//if (bLogDebugMessages) LOGMSG(NonPlayerChar, Warning, "NPCs without Friendly Fire enabled cannot attack other NPCs");

			return;
		}

		if (bLogDebugMessages) LOGMSG(NonPlayerChar, Log, "Performing melee strike on: " + ActorToHit->GetName());

		SimulateMeleeStrike();

		/* Apply damage directly if no melee animation is set */
		if (!MeleeAnimMontage)
		{
			DealMeleeDamageTo(ActorToHit);
		}

		/* Set re-trigger timer to re-check overlapping pawns and attack at melee attack rate interval */
		GetWorldTimerManager().SetTimer(
			TimerHandle_MeleeAttack, this, &AGFPSAICharacter::RetriggerMeleeStrike, MeleeStrikeCooldown, false);
	}
}

void AGFPSAICharacter::DealMeleeDamageTo(AActor* ActorToDmg)
{
	if (!ActorToDmg)
	{
		LOGMSG(NonPlayerChar, Warning, "ActorToDmg was null");
		return;
	}

	FPointDamageEvent DmgEvent;
	DmgEvent.DamageTypeClass = MeleeDamageType;
	DmgEvent.Damage = MeleeDamageAmount;

	//@??? Should MeleeDamageType be nullchecked here?
	float DamageInflicted = ActorToDmg->TakeDamage(DmgEvent.Damage, DmgEvent, GetController(), this);

	if (bLogDebugMessages) LOGMSG_M(NonPlayerChar, Log, "Inflicted [%f] damage on: %s", DamageInflicted, *ActorToDmg->GetName());

}

void AGFPSAICharacter::SimulateMeleeStrike()
{
	/* Melee Animation Montage */
	if (bLogDebugMessages) LOGMSG(NonPlayerChar, Log, /* if MeleeAnimMontage is set, log its name, else log "NOT FOUND" */
		"Playing Animation Montage: " + ((MeleeAnimMontage) ? MeleeAnimMontage->GetName() : "[MeleeAnimMontage NOT FOUND]"));

	PlayAnimMontage(MeleeAnimMontage);

	/* Melee Attack Sound */
	if (bLogDebugMessages) LOGMSG(NonPlayerChar, Log, /* if MeleeAnimMontage is set, log its name, else log "NOT FOUND" */
		"Playing Character Sound: " + ((SoundAttackMelee) ? SoundAttackMelee->GetName() : "[SoundAttackMelee NOT FOUND]"));

	PlayCharacterSound(SoundAttackMelee);
}

void AGFPSAICharacter::RetriggerMeleeStrike()
{
	/* Re-check for BaseCharacters in melee range */
	TArray<AActor*> Overlaps;
	MeleeCollisionComp->GetOverlappingActors(Overlaps, AGFPSBaseCharacter::StaticClass());

	/* Cancel attack if no BaseCharacters are in melee range */
	if (Overlaps.Num() == 0)
	{
		if (bLogDebugMessages) LOGMSG(NonPlayerChar, Log, "No BaseCharacters in melee range...cancelling melee attack");

		return;
	}

	/* Perform melee strike on each BaseCharacter in melee range */
	for (int32 i = 0; i < Overlaps.Num(); i++)
	{
		AGFPSBaseCharacter* CharInRange = Cast<AGFPSBaseCharacter>(Overlaps[i]);
		if (CharInRange)
		{
			//Clear any Melee Attack timers that have been set (required to attack multiple characters at once)
			GetWorldTimerManager().ClearTimer(TimerHandle_MeleeAttack);

			PerformMeleeStrikeOn(CharInRange);
			//break; /* Break to only attack one character maximum */
		}
	}
}

/***************************************************************************************************************************/
/***** Sound ***** Sound ***** Sound ***** Sound ***** Sound ***** Sound ***** Sound ***** Sound ***** Sound ***************/
/***************************************************************************************************************************/
/*                                                                                                                         */

void AGFPSAICharacter::UpdateAudioLoop(bool bNewSensedTarget)
{
	/* Start playing the chasing sound and the "noticed player" sound if the state is about to change */
	if (bNewSensedTarget && !bSensedTarget)
	{
		PlayCharacterSound(SoundPlayerNoticed);

		AudioLoopComp->SetSound(SoundChasing);
		AudioLoopComp->Play();
	}
	else
	{
		if (BotType == EBotBehaviorType::Patrol)
		{
			AudioLoopComp->SetSound(SoundPatrolling);
			AudioLoopComp->Play();
		}
		else //if (BotType == EBotBehaviorType::Idle)
		{
			AudioLoopComp->SetSound(SoundIdle);
			AudioLoopComp->Play();
		}
	}
}

UAudioComponent* AGFPSAICharacter::PlayCharacterSound(USoundCue* CueToPlay)
{
	if (CueToPlay)
	{
		if (bLogDebugMessages) LOGMSG(NonPlayerChar, Log, "Spawning Sound Attached: " + CueToPlay->GetName());

		return UGameplayStatics::SpawnSoundAttached(
			CueToPlay, RootComponent, NAME_None, FVector::ZeroVector, EAttachLocation::SnapToTarget, true);
	}

	return nullptr;
}

void AGFPSAICharacter::PlayHit(
	float DamageTaken, struct FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser, bool bKilled)
{
	Super::PlayHit(DamageTaken, DamageEvent, PawnInstigator, DamageCauser, bKilled);

	/* Stop playing the chasing sound */
	if (AudioLoopComp && bKilled)
	{
		AudioLoopComp->Stop();
	}
}

