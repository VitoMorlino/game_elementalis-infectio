// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GFPSBasePickupActor.generated.h"

UCLASS()
class GFPS_API AGFPSBasePickupActor : public AActor
{
	GENERATED_BODY()
	
public:	

	// Sets default values for this actor's properties
	AGFPSBasePickupActor();

	/* Enable this per-instance to allow logging of debugging messages for this PickupActor */
	UPROPERTY(EditAnywhere, Category = "Pickup|D E B U G G I N G")
	bool bLogDebugMessages;

	/* This is set automatically when this PickupActor is spawned or picked up */
	UPROPERTY(VisibleAnywhere, Category = "Pickup")
	bool bIsActive = false;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** Static Mesh Component */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Pickup", meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* MeshComp;

	/* Initial Location and Rotation */
	FTransform InitialTransform;

	/** Immediately spawn on begin play?
	*	If true, PickupActor will be made active on BeginPlay()
	*	If false, PickupActor will be hidden until RespawnPickup() is called on it (see note)
	*		NOTE: AllowRespawn does NOT have to be true when calling RespawnPickup() directly */
	UPROPERTY(EditAnywhere, Category = "Pickup")
	bool bStartActive = true;

	/* The Sound played when this PickupActor is picked up */
	UPROPERTY(EditDefaultsOnly, Category = "Pickup")
	class USoundBase* PickupSound;

	/** Will this PickupActor ever respawn? (NOTE: Calling RespawnPickup() directly will ignore this)
	*	If true, PickupActor will respawn after RespawnDelayAfterPickup + RespawnDelayRange
	*	If false, PickupActor will be destroyed OnPickup */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup|Respawn")
	bool bAllowRespawnOnPickup = true;

	/* Time (in seconds) to delay respawn after OnPickup() is called */
	UPROPERTY(EditAnywhere, Category = "Pickup|Respawn")
	float RespawnDelayAfterPickup = 5.f;

	/* Extra delay randomly applied on the respawn timer */
	UPROPERTY(EditAnywhere, Category = "Pickup|Respawn")
	float RespawnDelayRange = 0.f;

	/** Time (in seconds) to delay respawn when the player was too close
	* to the spawn location when this PickupActor tried to spawn
	*
	* If zero, force spawn anyway (see note)
	*	NOTE: A forced spawn can cause the player to get stuck or launched*/
	UPROPERTY(EditAnywhere, Category = "Pickup|Respawn")
	float SpawnFixDelay = 1.f;

	/* The Sound played when this PickupActor is spawned */
	UPROPERTY(EditDefaultsOnly, Category = "Pickup|Respawn")
	USoundBase* SpawnSound;

	/** Respawn this PickupActor at its InitialTransform (Not affected by bAllowRespawnOnPickup)
	*	
	*	Automatically called on BeginPlay() IF bStartActive 
	*	Automatically called after respawn timer expires IF bAllowRespawnOnPickup is true
	*
	*	Blueprint Event - ALWAYS CALL PARENT FUNCTION BY RIGHT-CLICKING
	*		THIS EVENT AND SELECTING "Add Call to Parent Function" */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Pickup|Respawn")
	void RespawnPickup();
	virtual void RespawnPickup_Implementation();

	/* Checks to see if something is blocking the spawn location */
	bool IsSpawnObstructed();

private:
	ECollisionEnabled::Type OriginalCollisionType;

public:	

	UFUNCTION(BlueprintCallable, Category = "Pickup")
	void Pickup(class AGFPSBaseCharacter* CharPickingUp);

	/** Called when Pickup is called on this PickupActor
	*
	*	Blueprint Event - ALWAYS CALL PARENT FUNCTION BY RIGHT-CLICKING THIS
	*		EVENT AND SELECTING "Add Call to Parent Function"
	*
	*	@param PawnThatPickedUp The pawn that picked up this PickupActor */
	UFUNCTION(BlueprintNativeEvent, Category = "Pickup")
	void OnPickup(class AGFPSBaseCharacter* CharThatPickedUp);
	virtual void OnPickup_Implementation(class AGFPSBaseCharacter* CharThatPickedUp);

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
};
