// Fill out your copyright notice in the Description page of Project Settings.

#include "GFPSBaseObstacle.h"
#include "Kismet/GameplayStatics.h"
#include "GFPSBaseCharacter.h"
#include "TimerManager.h"
#include "Sound/SoundCue.h"
#include "VitosLogMacros.h"

#include "Components/StaticMeshComponent.h"


// Sets default values
AGFPSBaseObstacle::AGFPSBaseObstacle()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Do not log debug messages by default...change this value per-instance, as necessary
	bLogDebugMessages = false;

	OnActorBeginOverlap.AddDynamic(this, &AGFPSBaseObstacle::OnBeginOverlap);
	OnActorEndOverlap.AddDynamic(this, &AGFPSBaseObstacle::OnEndOverlap);

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	MeshComp->OnComponentHit.AddDynamic(this, &AGFPSBaseObstacle::OnMeshHit);
	RootComponent = MeshComp;

	bIsActive = true;
	bApplyEffectOnHit = true;
	bEffectLoops = false;
	TimeBetweenLoops = 3.f;
	OnHitCooldown = 1.f;
}

// Called when the game starts or when spawned
void AGFPSBaseObstacle::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AGFPSBaseObstacle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/* Print on-screen debug messages while timers are active */
	if (bLogDebugMessages)
	{
		/* Loop Effect Timer is active */
		if (GetWorldTimerManager().IsTimerActive(TimerHandle_LoopEffect))
		{
			SCREENMSG_F(.1f, RED, "LoopEffect_TimeRemaining:",
				GetWorldTimerManager().GetTimerRemaining(TimerHandle_LoopEffect));
		}
	}
}

/***************************************************************************************************************************/
/***** Effect ***** Effect ***** Effect ***** Effect ***** Effect ***** Effect ***** Effect ***** Effect ***** Effect ******/
/***************************************************************************************************************************/
/*                                                                                                                         */

void AGFPSBaseObstacle::ApplyEffectTo(AActor* OtherActor)
{
	if (bLogDebugMessages) LOGMSG_M(Obstacle, Log, "Applying Effect To [%s]...", *GetNameSafe(OtherActor));

	/* Do nothing if the actor is a BaseChar and they are not alive */
	AGFPSBaseCharacter* BaseChar = Cast<AGFPSBaseCharacter>(OtherActor);
	if (BaseChar && !BaseChar->IsAlive())
	{
		if (bLogDebugMessages) LOGMSG_M(Obstacle, Warning,
			"Base Character [%s] is not alive...Cannot Apply Effect", *GetNameSafe(BaseChar));
		return;
	}

	//Play Effect Sound if it's set
	if (EffectSound)
	{
		UGameplayStatics::SpawnSoundAttached(
			EffectSound, RootComponent, NAME_None, FVector::ZeroVector, EAttachLocation::SnapToTarget, true);
	}

	/* Notify Blueprint we are applying the effect */
	BPApplyEffect(OtherActor);

	if (bLogDebugMessages) LOGMSG_M(Obstacle, Log, "...Finished Applying Effect To [%s]", *GetNameSafe(OtherActor));
}

void AGFPSBaseObstacle::BPApplyEffect_Implementation(AActor* OtherActor)
{
	//Nothing to do here...Just telling Blueprint we're applying the effect
}

void AGFPSBaseObstacle::ApplyEffectToAllOverlaps()
{
	if (bLogDebugMessages) LOGMSG_M(Obstacle, Log, "Applying Effect to each Actor overlapping Obstacle [%s]...", *GetName());

	if (!bIsActive)
	{
		if (bLogDebugMessages) LOGMSG(Obstacle, Warning, "bIsActive is false...Cancelling ApplyEffectToAllOverlaps");
		return;
	}

	if (OverlappingActors.Num() <= 0)
	{
		if (bLogDebugMessages) LOGMSG(Obstacle, Warning, "No OverlappingActors...Cancelling ApplyEffectToAllOverlaps");
		return;
	}

	/* Loop through and call ApplyEffectTo on each overlapping actor */
	for (int32 i = 0; i < OverlappingActors.Num(); i++)
	{
		// [CRASH-DEBUGGING] Skip to the next actor if the actor is a nullptr
		if (!OverlappingActors[i])
		{
			if (bLogDebugMessages) LOGMSG(Obstacle, Error,
				"A Null Pointer was found in OverlappingActors...Cannot Apply Effect to: [nullptr]");
			continue;
		}

		/* Apply effect to the actor if they are not in the array of RecentlyHitActors */
		if (!RecentlyHitActors.Contains(OverlappingActors[i]))
		{
			ApplyEffectTo(OverlappingActors[i]);
		}
		else
		{
			if (bLogDebugMessages) LOGMSG_M(Obstacle, Warning,
				"Obstacle [%s] was recently hit by Actor [%s]...Skipping effect (this time)",
				*GetName(), *GetNameSafe(OverlappingActors[i]));
		}
	}

	if (bEffectLoops)
	{
		/* Start TimerHandle_LoopEffect and call ApplyEffectToAllOverlaps() again when it ends */
		GetWorldTimerManager().SetTimer(
			TimerHandle_LoopEffect, this, &AGFPSBaseObstacle::ApplyEffectToAllOverlaps, TimeBetweenLoops, false);
	}

	if (bLogDebugMessages) LOGMSG_M(Obstacle, Log, "...Finished Applying Effect to each Actor overlapping Obstacle [%s]", *GetName());
}

/***************************************************************************************************************************/
/***** Hit ***** Hit ***** Hit ***** Hit ***** Hit ***** Hit ***** Hit ***** Hit ***** Hit ***** Hit ***** Hit *************/
/***************************************************************************************************************************/
/*                                                                                                                         */

void AGFPSBaseObstacle::OnMeshHit(
	UPrimitiveComponent* HitComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComponent,
	FVector NormalImpulse,
	const FHitResult& Hit)
{
	/* Do nothing if this obstacle is not active */
	if (!bApplyEffectOnHit)
	{
		if (bLogDebugMessages) LOGMSG_M(Obstacle, Warning,
			"Obstacle [%s] does not Apply Effect On Hit...Ignoring this OnHit trigger", *GetName());
		return;
	}

	/* Do nothing if this obstacle is not active */
	if (!bIsActive)
	{
		if (bLogDebugMessages) LOGMSG_M(Obstacle, Warning,
			"Obstacle [%s] is not active...Ignoring this OnHit trigger", *GetName());
		return;
	}

	/* Apply Effect To the OtherActor if they're not already overlapping the obstacle */
	if (!OverlappingActors.Contains(OtherActor))
	{
		/** Add the actor to the array of RecentlyHitActors
		*	bNewHit is false if the OtherActor was already in the array */
		bool bNewHit = RecentlyHitActors.Num() == RecentlyHitActors.AddUnique(OtherActor);

		if (bNewHit)
		{
			if (bLogDebugMessages) LOGMSG_M(Obstacle, Log, "Obstacle [%s] was hit by Actor [%s]",
				*GetName(), *GetNameSafe(OtherActor));

			ApplyEffectTo(OtherActor);

			/* Play the HitSound if it's set */
			if (HitSound)
			{
				UGameplayStatics::SpawnSoundAttached(
					HitSound, RootComponent, NAME_None, FVector::ZeroVector, EAttachLocation::SnapToTarget, true);
			}

			/* Start timer and call RemoveFirstHitActor() when it ends */
			FTimerHandle TimerHandle_OnHitCooldown;
			GetWorldTimerManager().SetTimer(
				TimerHandle_OnHitCooldown, this, &AGFPSBaseObstacle::RemoveFirstHitActor, OnHitCooldown, false);
		}
		else
		{
			if (bLogDebugMessages) LOGMSG_M(Obstacle, Warning,
				"Obstacle [%s] was recently hit by Actor [%s]...Cannot apply effect until OnHitCooldown expires",
				*GetName(), *GetNameSafe(OtherActor));
		}
	}
	else //if OverlappingActors contains the OtherActor
	{
		if (bLogDebugMessages) LOGMSG_M(Obstacle, Warning,
			"Actor [%s] is already overlapping...Ignoring this OnHit trigger", *GetNameSafe(OtherActor));
	}
}

void AGFPSBaseObstacle::RemoveFirstHitActor()
{
	/* Remove the first actor in the array of RecentlyHitActors */
	RecentlyHitActors.RemoveAt(0);
}

/***************************************************************************************************************************/
/***** Overlap ***** Overlap ***** Overlap ***** Overlap ***** Overlap ***** Overlap ***** Overlap ***** Overlap ***********/
/***************************************************************************************************************************/
/*                                                                                                                         */

void AGFPSBaseObstacle::OnBeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	/* Do nothing if this obstacle is not active */
	if (!bIsActive)
	{
		if (bLogDebugMessages) LOGMSG_M(Obstacle, Warning,
			"Obstacle [%s] is not active...Ignoring this OnOverlap trigger", *GetName());
		return;
	}

	/** Add the actor that overlapped this obstacle to the OverlappingActors array
	*	bNewOverlap is false if the OtherActor was already in the array */
	bool bNewOverlap = OverlappingActors.Num() == OverlappingActors.AddUnique(OtherActor);

	/* Only Do The Thing if the actor that overlapped wasn't already overlapping */
	if (bNewOverlap)
	{
		if (bLogDebugMessages) LOGMSG_M(Obstacle, Log,
			"Obstacle [%s] was overlapped by Actor [%s]", *GetName(), *GetNameSafe(OtherActor));

		/* Call ApplyEffectToAllOverlaps if TimerHandle_LoopEffect is not already active */
		if (!GetWorldTimerManager().IsTimerActive(TimerHandle_LoopEffect))
		{
			ApplyEffectToAllOverlaps();
		}
	}
	else
	{
		if (bLogDebugMessages) LOGMSG_M(Obstacle, Warning,
			"Uh Oh! Obstacle [%s] was overlapped by [%s]...but they were already in the array of OverlappingActors",
			*GetName(), *GetNameSafe(OtherActor));
	}
}

void AGFPSBaseObstacle::OnEndOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	int32 ActorIndex = OverlappingActors.Find(OtherActor);

	if (ActorIndex == INDEX_NONE)
	{
		if (bLogDebugMessages) LOGMSG_M(Obstacle, Warning,
			"Uh oh! Actor [%s] was not found in the array of OverlappingActors", *GetNameSafe(OtherActor));
		return;
	}
	else
	{
		OverlappingActors.RemoveAt(ActorIndex);

		if (bLogDebugMessages) LOGMSG_M(Obstacle, Log,
			"Actor [%s] is no longer overlapping Obstacle [%s]", *GetNameSafe(OtherActor), *GetName());
	}
}

