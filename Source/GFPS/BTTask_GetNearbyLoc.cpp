// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_GetNearbyLoc.h"
#include "GFPSAIController.h"
#include "GFPSAIWaypoint.h"
#include "GFPSAICharacter.h"
#include "AI/Navigation/NavigationSystem.h"

/* AI Module includes */
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
/* This includes all key types like UBlackboardKeyType_Vector used below. */
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"

EBTNodeResult::Type UBTTask_GetNearbyLoc::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	AGFPSAIController* MyController = Cast<AGFPSAIController>(OwnerComp.GetAIOwner());
	if (!MyController)
	{
		LOGMSG(NonPlayerCon, Error, "AIController not found");
		return EBTNodeResult::Failed;
	}

	bool bLogDebug = MyController->bLogDebugMessages;

	AGFPSAICharacter* AIChar = Cast<AGFPSAICharacter>(MyController->GetCharacter());
	if (!AIChar)
	{
		if (bLogDebug) LOGMSG(NonPlayerCon, Error, "AICharacter not found");
		return EBTNodeResult::Failed;
	}

	UNavigationSystem* NavSys = GetWorld()->GetNavigationSystem();
	const FVector SearchOrigin = MyController->GetLocToGo();
	const float SearchRadius = AIChar->MaxWanderDistance;
	FNavLocation NavLoc;
	bool Success = false;

	if (NavSys)
	{
		Success = NavSys->GetRandomReachablePointInRadius(SearchOrigin, SearchRadius, NavLoc);
	}

	if (Success)
	{
		/* The selected key should be "NearbyLoc" in the BehaviorTree setup */
		OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(BlackboardKey.GetSelectedKeyID(), NavLoc.Location);

		if (bLogDebug) LOGMSG(NonPlayerCon, Log, "Successfully set NearbyLoc");

		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}
