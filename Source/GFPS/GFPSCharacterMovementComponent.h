// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GFPSCharacterMovementComponent.generated.h"

/**
 * This custom CharacterMovementComponent overrides GetMaxSpeed() to support additional movement speeds such as Sprinting
 */
UCLASS()
class GFPS_API UGFPSCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

	/* Overriden to multiply speed by sprint speed multiplier */
	virtual float GetMaxSpeed() const override;

	/** Notification that the character is stuck in geometry.
	*	Only called during walking movement. */
	virtual void OnCharacterStuckInGeometry(const FHitResult* Hit) override;

	/** Handle a blocking impact. Calls ApplyImpactPhysicsForces for the hit, if bEnablePhysicsInteraction is true. */
	virtual void HandleImpact(const FHitResult& Hit, float TimeSlice = 0.f, const FVector& MoveDelta = FVector::ZeroVector) override;

	/** Apply physics forces to the impacted component, if bEnablePhysicsInteraction is true.
	* @param Impact				HitResult that resulted in the impact
	* @param ImpactAcceleration	Acceleration of the character at the time of impact
	* @param ImpactVelocity		Velocity of the character at the time of impact */
	virtual void ApplyImpactPhysicsForces(const FHitResult& Impact, const FVector& ImpactAcceleration, const FVector& ImpactVelocity) override;

public:
	UGFPSCharacterMovementComponent();

	
};
