// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "GFPSGameMode.h"
#include "GFPSHUD.h"
#include "GFPSCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGFPSGameMode::AGFPSGameMode() : Super()
{
	DefaultPlayerName = DefaultPlayerName.FromName("TestSubject");

	// Try to set DefaultPawnClass to the Blueprinted class, but default to the base c++ class if not found 
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Player/BP_GFPSCharacter"));
	DefaultPawnClass = (PlayerPawnBPClass.Class) ? PlayerPawnBPClass.Class : TSubclassOf<APawn>(AGFPSCharacter::StaticClass());

	// use our custom HUD class
	HUDClass = AGFPSHUD::StaticClass();
}
