// Fill out your copyright notice in the Description page of Project Settings.

#include "GFPSBaseCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"
#include "Sound/SoundCue.h"
#include "GFPSBaseDamageType.h"
#include "VitosLogMacros.h"

//Component Includes
#include "GFPSCharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/PawnNoiseEmitterComponent.h"


// Sets default values
AGFPSBaseCharacter::AGFPSBaseCharacter(const FObjectInitializer& ObjectInitializer)
/* Override the movement class from the base class to our own to support multiple speeds (eg. sprinting) */
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UGFPSCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Do not log debug messages by default...change this value per-instance, as necessary
	bLogDebugMessages = false;

	bInCombat = false;
	InCombatDuration = 5.f;

	Health = 100.f; // Set to a default value of 100...can override in child class
	AutoHealInterval = 2.f; // Autoheal every 2 seconds by default
	AutoHealPerTick = 5.f; // Autoheal by 5 every AutoHeal tick by default
	AutoHealPerTickOOC = 20.f; // Autoheal by 20 every AutoHeal tick when out of combat by default
	SprintingSpeedMultiplier = 2.f; //Sprinting doubles MaxSpeed by default...can override in child class

	/** Noise emitter for both players and enemies - Tracks Noise Data used by PawnSensingComponent
	*	This is required for any MakeNoise() calls from this pawn to be detected by a PawnSensingComponent */
	NoiseEmitterComp = CreateDefaultSubobject<UPawnNoiseEmitterComponent>(TEXT("NoiseEmitterComp"));
}

/***************************************************************************************************************************/
/***** Condition ***** Condition ***** Condition ***** Condition ***** Condition ***** Condition ***** Condition ***********/
/***************************************************************************************************************************/
/*                                                                                                                         */

float AGFPSBaseCharacter::GetMaxHealth() const
{
	// Retrieve the default value of the health property that is assigned on instantiation.
	return GetClass()->GetDefaultObject<AGFPSBaseCharacter>()->Health;
}

float AGFPSBaseCharacter::GetHealth() const
{
	return Health;
}

void AGFPSBaseCharacter::AutoHeal()
{
	if (Health < GetMaxHealth())
	{
		if (bInCombat)
		{
			HealBy(AutoHealPerTick);
		}
		else
		{
			HealBy(AutoHealPerTickOOC);
		}

		if (bLogDebugMessages) LOGMSG_M(BaseChar, Log,
			"BaseCharacer [%s] AutoHealed - Current Health: [%d/%d]",
			*GetName(), (int32)Health, (int32)GetMaxHealth());
	}
	else
	{
		if (bLogDebugMessages) LOGMSG_M(BaseChar, Log,
			"Base Character [%s] reached MaxHealth - Cancelling AutoHeal",
			*GetName());

		GetWorldTimerManager().ClearTimer(TimerHandle_AutoHeal);
	}
}

void AGFPSBaseCharacter::HealBy(float HealAmount)
{
	Health += HealAmount;

	if (bLogDebugMessages) LOGMSG_M(BaseChar, Log,
		"Base Character [%s] Healed By [%f]",
		*GetName(), HealAmount);

	if (Health > GetMaxHealth()) Health = GetMaxHealth();
}

void AGFPSBaseCharacter::StartCombat()
{
	bInCombat = true;

	GetWorldTimerManager().SetTimer(
		TimerHandle_InCombat, this, &AGFPSBaseCharacter::EndCombat, 5.f, false);

	if (bLogDebugMessages) LOGMSG_M(BaseChar, Log, "BaseCharacter [%s] is In-Combat!", *GetName());
}

void AGFPSBaseCharacter::EndCombat()
{
	bInCombat = false;

	if (bLogDebugMessages) LOGMSG_M(BaseChar, Log, "BaseCharacter [%s] is no longer In-Combat!", *GetName());
}

bool AGFPSBaseCharacter::IsAlive() const
{
	return Health > 0;
}

/***************************************************************************************************************************/
/***** Movement ***** Movement ***** Movement ***** Movement ***** Movement ***** Movement ***** Movement ******************/
/***************************************************************************************************************************/
/*                                                                                                                         */

bool AGFPSBaseCharacter::IsSprinting() const
{
	if (!GetCharacterMovement())
	{
		return false;
	}

	// Don't allow sprint while strafing sideways or standing still 
	return bWantsToSprint && !GetVelocity().IsZero()
		/** 0.1 allows for diagonal sprinting. (holding W+A or W+D keys)
		*	(1.0 is straight forward, -1.0 is backward while near 0 is sideways or standing still) */
		&& (FVector::DotProduct(GetVelocity().GetSafeNormal2D(), GetActorRotation().Vector()) > 0.1);
}

void AGFPSBaseCharacter::SetSprinting(bool NewSprinting)
{
	bWantsToSprint = NewSprinting;

	if (bIsCrouched && bWantsToSprint)
	{
		if (bLogDebugMessages) LOGMSG(BaseChar, Warning, "Uncrouching to start sprinting");

		UnCrouch();
	}

	if (bLogDebugMessages) LOGMSG_2(BaseChar, Log, GetName(), bWantsToSprint ? "Started Sprinting" : "Stopped Sprinting");
}

float AGFPSBaseCharacter::GetSprintingSpeedMultiplier() const
{
	return SprintingSpeedMultiplier;
}

/***************************************************************************************************************************/
/***** Damage & Death ***** Damage & Death ***** Damage & Death ***** Damage & Death ***** Damage & Death ******************/
/***************************************************************************************************************************/
/*                                                                                                                         */

float AGFPSBaseCharacter::TakeDamage(
	float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (Health <= 0.f) return 0.f;

	/** Here is where we would modify the damage based on rules in GameMode
	*	//ACustomGameMode* MyGameMode = Cast<ACustomGameMode>(GetWorld()->GetAuthGameMode());
	*	//Damage = MyGameMode ? MyGameMode->ModifyDamage(Damage, this, DamageEvent, EventInstigator, DamageCauser) : Damage;
	*/


	UGFPSBaseDamageType* DmgType = Cast<UGFPSBaseDamageType>(DamageEvent.DamageTypeClass->GetDefaultObject());
	if (DmgType)
	{
		float DamageResisted = Damage *
			DamageResistances.GetResistanceTo(DmgType->GetBasicDamageType(), DmgType->GetElementalDamageType(), bLogDebugMessages);

		Damage -= DamageResisted;

		if (bLogDebugMessages) LOGMSG_F(BaseChar, Log, "Amount of Damage Resisted:", DamageResisted);
	}
	else
	{
		if (bLogDebugMessages) LOGMSG(BaseChar, Error, "DamageEvent.DamageTypeClass was not BaseDamageType");
	}

	//If the amount of Damage is enough to kill
	if (Damage >= Health)
	{
		bool bDamageCanKill = true;

		//Check if the DmgType can kill
		if (DamageEvent.DamageTypeClass)
		{
			if (DmgType)
			{
				bDamageCanKill = DmgType->GetCanKill();
			}
		}
		else
		{
			if (bLogDebugMessages) LOGMSG(LogTemp, Warning, "DamageEvent.DamageTypeClass not found");
		}

		//If the DmgType CAN NOT kill
		if (!bDamageCanKill)
		{
			if (bLogDebugMessages) LOGMSG_M(LogTemp, Warning,
				"DmgType can not kill...Mitigating Damage from [%f] to [%f]",
				Damage, Health - 0.1f);

			//Make Damage only enough to get to 0.1 Health
			Damage = Health - 0.1f;
		}
	}

	const float ActualDamage = Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	if (ActualDamage > 0.f)
	{
		Health -= ActualDamage;

		if (bLogDebugMessages) LOGMSG_M(BaseChar, Warning, "Character took [%f] damage...Health Remaining: [%f]",
			ActualDamage, FMath::Max(0.f, Health));

		if (Health <= 0)
		{
			Die(ActualDamage, DamageEvent, EventInstigator, DamageCauser);
		}
		else
		{
			if (DmgType)
			{
				//Notify Blueprint classes about the damage we took
				BPEventTakeDamage(Damage, DmgType, EventInstigator, DamageCauser);
			}

			/* Shorthand for...if X ? Pick1 :else: Pick2 */
			APawn* InstigatorPawn = EventInstigator ? EventInstigator->GetPawn() : nullptr;

			PlayHit(ActualDamage, DamageEvent, InstigatorPawn, DamageCauser, false);

			/* Start Combat on this BaseCharacter */
			StartCombat();

			/* Start Combat on the InstigatorPawn if they are a BaseCharacter */
			AGFPSBaseCharacter* InstigatorBaseChar = Cast<AGFPSBaseCharacter>(InstigatorPawn);
			if (InstigatorBaseChar && InstigatorBaseChar != this)
			{
				InstigatorBaseChar->StartCombat();
			}

			/* If it's not already active, set the timer to start autohealing */
			if (!GetWorldTimerManager().IsTimerActive(TimerHandle_AutoHeal))
			{
				GetWorldTimerManager().SetTimer(
					TimerHandle_AutoHeal, this, &AGFPSBaseCharacter::AutoHeal, AutoHealInterval, true);

				if (bLogDebugMessages) LOGMSG(BaseChar, Log, "Starting AutoHeal...");
			}
			else
			{
				if (bLogDebugMessages) LOGMSG(BaseChar, Log, "AutoHeal already in progress...");
			}
		}
	}

	return ActualDamage;
}

void AGFPSBaseCharacter::BPEventTakeDamage_Implementation(
	float Damage, UGFPSBaseDamageType const* BaseDamageType, AController* EventInstigator, AActor* DamageCauser)
{
	//Nothing to do here, we're just telling Blueprint classes about the damage we took
}

void AGFPSBaseCharacter::PlayHit(
	float DamageTaken, FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser, bool bKilled)
{
	// If the hit killed this character, play its SoundDeath
	if (bKilled && SoundDeath)
	{
		UGameplayStatics::SpawnSoundAttached(
			SoundDeath, RootComponent, NAME_None, FVector::ZeroVector, EAttachLocation::SnapToTarget, true);
	}
	// If the hit didn't kill this character, play its SoundTakehit
	else if (SoundTakeHit)
	{
		UGameplayStatics::SpawnSoundAttached(
			SoundTakeHit, RootComponent, NAME_None, FVector::ZeroVector, EAttachLocation::SnapToTarget, true);
	}
}

bool AGFPSBaseCharacter::CanDie(
	float KillingDamage, FDamageEvent const & DamageEvent, AController * Killer, AActor * DamageCauser) const
{
	/* Check if character is already dying or being destroyed */
	if (bIsDying ||
		IsPendingKill() ||
		GetWorld()->GetAuthGameMode() == nullptr)
	{
		return false;
	}

	return true;
}

bool AGFPSBaseCharacter::Die(
	float KillingDamage, FDamageEvent const & DamageEvent, AController * Killer, AActor * DamageCauser)
{
	if (!CanDie(KillingDamage, DamageEvent, Killer, DamageCauser))
	{
		if (bLogDebugMessages) LOGMSG(BaseChar, Warning, "CanDie() returned false");

		return false;
	}

	if (Health < 0) Health = 0;

	/* Fallback to default DamageType if none is specified */
	UDamageType const* const DamageType = DamageEvent.DamageTypeClass ?
		DamageEvent.DamageTypeClass->GetDefaultObject<UDamageType>() : GetDefault<UDamageType>();

	Killer = GetDamageInstigator(Killer, *DamageType);

	/* Here is where we would notify the gamemode we got killed for scoring and game-over state */
	//AController* KilledPlayer = Controller ? Controller : Cast<AController>(GetOwner());
	//GetWorld()->GetAuthGameMode<ACustomGameMode>()->Killed(Killer, KilledPlayer, this, DamageType);

	/* Clear any timers running on this character */
	GetWorldTimerManager().ClearAllTimersForObject(this);

	OnDeath(KillingDamage, DamageEvent, Killer ? Killer->GetPawn() : nullptr, DamageCauser);

	return true;
}

void AGFPSBaseCharacter::OnDeath_Implementation(
	float KillingDamage, FDamageEvent const & DamageEvent, APawn * PawnInstigator, AActor * DamageCauser)
{
	if (bIsDying)
	{
		return;
	}

	if (bLogDebugMessages) LOGMSG(BaseChar, Warning, "** Character Died **");

	bReplicateMovement = false;
	bTearOff = true;
	bIsDying = true;

	PlayHit(KillingDamage, DamageEvent, PawnInstigator, DamageCauser, true);

	DetachFromControllerPendingDestroy(); //@note This prevented the camera from following a dead ragdoll

	/* Disable all collision on capsule */
	UCapsuleComponent* CapsuleComp = GetCapsuleComponent();
	CapsuleComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	CapsuleComp->SetCollisionResponseToAllChannels(ECR_Ignore);

	USkeletalMeshComponent* Mesh3P = GetMesh();
	if (Mesh3P)
	{
		Mesh3P->SetCollisionProfileName(TEXT("Ragdoll"));
	}
	SetActorEnableCollision(true);

	SetRagdollPhysics();
}

void AGFPSBaseCharacter::SetRagdollPhysics()
{
	bool bInRagdoll = false;
	USkeletalMeshComponent* Mesh3P = GetMesh();

	if (IsPendingKill())
	{
		bInRagdoll = false;
	}
	else if (!Mesh3P || !Mesh3P->GetPhysicsAsset())
	{
		bInRagdoll = false;
	}
	else
	{
		Mesh3P->SetAllBodiesSimulatePhysics(true);
		Mesh3P->SetSimulatePhysics(true);
		Mesh3P->WakeAllRigidBodies();
		Mesh3P->bBlendPhysics = true;

		bInRagdoll = true;
	}

	UCharacterMovementComponent* MoveComp = Cast<UCharacterMovementComponent>(GetMovementComponent());
	if (MoveComp)
	{
		if (bLogDebugMessages) LOGMSG(BaseChar, Log, "Disabling Character Movement and Tick() in the movement component");

		MoveComp->StopMovementImmediately();
		MoveComp->DisableMovement();
		MoveComp->SetComponentTickEnabled(false);
	}

	if (!bInRagdoll)
	{
		// Immediately hide the pawn
		TurnOff();
		SetActorHiddenInGame(true);
		SetLifeSpan(1.0f);

		if (bLogDebugMessages) LOGMSG(BaseChar, Warning, "Ragdoll Failed...Destroying Character in 1 second");
	}
	else
	{
		SetLifeSpan(5.0f);

		if (bLogDebugMessages) LOGMSG(BaseChar, Warning, "Ragdoll Succeeded...Destroying Character in 5 seconds");
	}
}

void AGFPSBaseCharacter::FellOutOfWorld(const UDamageType & DmgType)
{
	Die(Health, FDamageEvent(DmgType.GetClass()), nullptr, nullptr);
}

