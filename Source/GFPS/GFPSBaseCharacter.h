// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CustomTypes.h"
#include "GFPSBaseCharacter.generated.h"

UCLASS()
class GFPS_API AGFPSBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	// Sets default values for this character's properties
	AGFPSBaseCharacter(const FObjectInitializer& ObjectInitializer); //ObjectInitializer required to use our custom character movement component

	/* Enable this per-instance to allow logging of debugging messages for this BaseCharacter */
	UPROPERTY(EditAnywhere, Category = "BaseCharacter|D E B U G G I N G")
	bool bLogDebugMessages;

private:

	/** Noise emitter for both players and enemies - Tracks Noise Data used by PawnSensingComponent
	*	This is required for any MakeNoise() calls from this pawn to be detected by a PawnSensingComponent */
	UPawnNoiseEmitterComponent* NoiseEmitterComp;

/*****************************************************************************/
/*      Sound                                                                */
/*****************************************************************************/
/*                                                                           */

public:

	/* The sound cue to play when this character takes damage that does not kill them */
	UPROPERTY(EditDefaultsOnly, Category = "BaseCharacter|Sound")
	class USoundCue* SoundTakeHit;

	/* The sound cue to play when this character takes damage that kills them */
	UPROPERTY(EditDefaultsOnly, Category = "BaseCharacter|Sound")
	class USoundCue* SoundDeath;

/*****************************************************************************/
/*		Condition                                                            */
/*****************************************************************************/
/*                                                                           */

public:

	/* Returns this character's Max Health (The amount of health they have at full-health) */
	UFUNCTION(BlueprintCallable, Category = "BaseCharacter|Condition")
	float GetMaxHealth() const;
		//Note: "const" here says this method should not alter member variables

	/* Returns this character's current Health value*/
	UFUNCTION(BlueprintCallable, Category = "BaseCharacter|Condition")
	float GetHealth() const;
		//Note: "const" here says this method should not alter member variables

	UFUNCTION(BlueprintCallable, Category = "BaseCharacter|Condition")
	void AutoHeal();

	UFUNCTION(BlueprintCallable, Category = "BaseCharacter|Condition")
	void HealBy(float HealAmount);

	UFUNCTION(BlueprintCallable, Category = "BaseCharacter|Condition")
	void StartCombat();

	UFUNCTION(BlueprintCallable, Category = "BaseCharacter|Condition")
	void EndCombat();

	/* Returns true if this character's Health is greater than zero*/
	UFUNCTION(BlueprintCallable, Category = "BaseCharacter|Condition")
	bool IsAlive() const;
		//Note: "const" here says this method should not alter member variables

protected:

	UPROPERTY(EditDefaultsOnly, Category = "BaseCharacter|Condition")
	float Health;

	/* Amount of healing per tick when InCombat */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseCharacter|Condition")
	float AutoHealPerTick;

	/* Amount of healing per tick when NOT InCombat */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseCharacter|Condition")
	float AutoHealPerTickOOC;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseCharacter|Condition")
	float AutoHealInterval;
	FTimerHandle TimerHandle_AutoHeal;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BaseCharacter|Condition")
	bool bInCombat;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseCharacter|Condition")
	float InCombatDuration;
	FTimerHandle TimerHandle_InCombat;

	/* Damage Boosts increase outgoing damage by percentage */
	//@todo Damage Boosts not yet implemented

	/* Damage Resistances decrease incoming damage by percentage */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseCharacter|Condition")
	FDamageResistance DamageResistances;

/*****************************************************************************/
/*		Movement                                                             */
/*****************************************************************************/
/*                                                                           */

public:

	/* Returns true if this character should be sprinting */
	UFUNCTION(BlueprintCallable, Category = "BaseCharacter|Movement")
	virtual bool IsSprinting() const;
		//Note: "const" here says this method should not alter member variables

	/* Update sprint state  */
	UFUNCTION(BlueprintCallable, Category = "BaseCharacter|Movement")
	virtual void SetSprinting(bool NewSprinting);

	float GetSprintingSpeedMultiplier() const;
		//Note: "const" here says this method should not alter member variables

protected:

	/* Movement speed is multiplied by this amount when sprinting */
	UPROPERTY(EditDefaultsOnly, Category = "BaseCharacter|Movement")
	float SprintingSpeedMultiplier;

	/* This is set internally when SetSprinting(true/false) is called */
	UPROPERTY(VisibleAnywhere, Category = "BaseCharacter|Movement")
	bool bWantsToSprint;

/*****************************************************************************/
/*		Damage & Death                                                       */
/*****************************************************************************/
/*                                                                           */

protected:

	bool bIsDying;

	/* Take damage & handle death */
	virtual float TakeDamage(
		float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

	/** Called when this character takes damage that does not kill them
	* @param Damage The amount of damage this character took (after resistance calculations)
	* @param BaseDamageType The type of damage this character took
	* @param EventInstigator The controller responsible for the damage this character took (e.g. the shooter of the projectile)
	* @param DamageCauser The Actor that directly caused the damage (e.g. the projectile that hit this character) */
	UFUNCTION(BlueprintNativeEvent)
	void BPEventTakeDamage(
		float Damage, class UGFPSBaseDamageType const* BaseDamageType, class AController* EventInstigator, class AActor* DamageCauser);
	void BPEventTakeDamage_Implementation(
		float Damage, class UGFPSBaseDamageType const* BaseDamageType, class AController* EventInstigator, class AActor* DamageCauser);

	virtual void PlayHit(
		float DamageTaken, struct FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser, bool bKilled);

	virtual bool CanDie(
		float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser) const;

	virtual bool Die(
		float KillingDamage, FDamageEvent const& DamageEvent, AController* Killer, AActor* DamageCauser);

	/** Called when this character dies (runs out of Health)
	*
	*	Blueprint Event - ALWAYS CALL PARENT FUNCTION BY RIGHT-CLICKING
	*		THIS EVENT AND SELECTING "Add Call to Parent Function"
	* @param KillingDamage The amount of damage that was applied in the final blow
	* @param DamageEvent Data package that fully describes the damage received (such as DamageType)
	* @param PawnInstigator The Pawn responsible for the final blow (e.g. the shooter of the projectile)
	* @param DamageCauser The Actor that directly caused the damage (e.g. the projectile that hit this character) */
	UFUNCTION(BlueprintNativeEvent, Category = "BaseCharacter|Condition")
	void OnDeath(float KillingDamage, FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser);
	virtual void OnDeath_Implementation(
		float KillingDamage, FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser);

	//Override this in PlayerCharacter and attach CameraBoom to Mesh so it follows
	virtual void SetRagdollPhysics();

	//Override this in PlayerCharacter and angle Camera to face downward
	virtual void FellOutOfWorld(const class UDamageType& DmgType) override;

};
