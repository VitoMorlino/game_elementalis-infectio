// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_GetNewWaypoint.h"
#include "GFPSAIController.h"
#include "GFPSAIWaypoint.h"
#include "GFPSAICharacter.h"
#include "Kismet/GameplayStatics.h"
#include "AI/Navigation/NavigationSystem.h"
#include "AI/Navigation/NavigationPath.h"
#include "VitosLogMacros.h"

/* AI Module includes */
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
/* This contains includes all key types like UBlackboardKeyType_Vector used below. */
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"

EBTNodeResult::Type UBTTask_GetNewWaypoint::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	AGFPSAIController* MyController = Cast<AGFPSAIController>(OwnerComp.GetAIOwner());
	if (!MyController)
	{
		LOGMSG(NonPlayerCon, Error, "AIController not found");
		return EBTNodeResult::Failed;
	}

	bool bLogDebug = MyController->bLogDebugMessages;

	AGFPSAICharacter* AIChar = Cast<AGFPSAICharacter>(MyController->GetCharacter());
	if (!AIChar)
	{
		if (bLogDebug) LOGMSG(NonPlayerCon, Error, "AICharacter not found");
		return EBTNodeResult::Failed;
	}

	AGFPSAIWaypoint* CurrentWaypoint = MyController->GetWaypoint();
	AActor* NewWaypoint = nullptr;

	TArray<AActor*> AllWaypoints;
	UGameplayStatics::GetAllActorsOfClass(MyController, AGFPSAIWaypoint::StaticClass(), AllWaypoints);
	TArray<AActor*> ReachableWaypoints;

	/* Iterate through all the waypoints in the current level */
	for (int32 i = 0; i < AllWaypoints.Num(); i++)
	{
		FVector PathStart = AIChar->GetActorLocation();
		FVector PathEnd = AllWaypoints[i]->GetActorLocation();
		UNavigationPath* NavPath = UNavigationSystem::FindPathToLocationSynchronously(GetWorld(), PathStart, PathEnd);

		/* Add the waypoint to the array of ReachableWaypoints if it's reachable */
		if (NavPath && NavPath->IsValid() && !NavPath->IsPartial())
		{
			ReachableWaypoints.AddUnique(AllWaypoints[i]);
		}
	}

	if (ReachableWaypoints.Num() == 0)
	{
		if (bLogDebug) LOGMSG(NonPlayerCon, Warning, "Could not find a reachable waypoint to set");
		return EBTNodeResult::Failed;
	}

	/* Find a new waypoint randomly by index (this can include the current waypoint) */
	/* For more complex or human AI, add some weights based on distance and other environmental conditions here */
	NewWaypoint = ReachableWaypoints[FMath::RandRange(0, ReachableWaypoints.Num() - 1)];

	/* Assign the new waypoint to the Blackboard */
	if (NewWaypoint)
	{
		/* The selected key should be "CurrentWaypoint" in the BehaviorTree setup */
		OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(BlackboardKey.GetSelectedKeyID(), NewWaypoint);

		if (bLogDebug) LOGMSG(NonPlayerCon, Log, "Successfully set new CurrentWaypoint");

		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}
